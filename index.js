require("sdk/ui/button/action").ActionButton({
  id: "builtwith-tab",
  label: "BuiltWith Tab",
  icon: "./icon-16.png",
  onClick: builtwith
});
 
function builtwith() {
  var tabs = require("sdk/tabs");
  var url = encodeURIComponent(tabs.activeTab.url);
  var target = "http://builtwith.com/?" + url;
  tabs.open({
    url: target,
    inBackground: true,
  });
}
